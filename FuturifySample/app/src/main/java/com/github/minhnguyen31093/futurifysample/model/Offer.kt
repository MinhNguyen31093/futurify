package com.github.minhnguyen31093.futurifysample.model

data class Offer(val image: String, val name: String, val rating: Float, val reviews: Int, val location: String, val locationDetail: String)