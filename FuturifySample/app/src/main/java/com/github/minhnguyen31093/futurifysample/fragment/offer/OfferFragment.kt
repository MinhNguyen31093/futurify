package com.github.minhnguyen31093.futurifysample.fragment.offer

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.minhnguyen31093.futurifysample.R
import com.github.minhnguyen31093.futurifysample.model.Offer
import kotlinx.android.synthetic.main.fragment_offer.*

class OfferFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_offer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEvent()
        setUpView()
    }

    private fun initEvent() {
        tvPrice.setOnClickListener {  }
        tvRating.setOnClickListener {  }
        tvOpenNow.setOnClickListener {  }
        tvHalal.setOnClickListener {  }
    }

    private fun setUpView() {
        val tabsName = resources.getStringArray(R.array.top_tab)
        for (tabName in tabsName) {
            val tab = getTab(tabName)
            tlTop.addTab(tab)
        }
        tvPrice.isSelected = true
        tvOpenNow.isSelected = true

        val items = ArrayList<Offer>()
        for (i in 1..10) {
            items.add(Offer("http://i65.tinypic.com/2vt1pgk.jpg", "Andrew's Kampung",
                    4f, 349, "Vegetarian · Asian · $$$",
                    "LG 49, Lower Ground Floor, Paradigm Mall"))
        }
        val adapter = OfferAdapter(items)
        rvOffer.adapter = adapter
    }

    private fun getTab(name: String): TabLayout.Tab {
        val tab = LayoutInflater.from(tlTop.context).inflate(R.layout.item_tab, tlTop, false)
        tab.findViewById<TextView>(R.id.tvTabName).text = name
        return tlTop.newTab().setCustomView(tab)
    }
}