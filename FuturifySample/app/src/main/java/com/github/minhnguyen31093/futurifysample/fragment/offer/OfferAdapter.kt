package com.github.minhnguyen31093.futurifysample.fragment.offer

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.github.minhnguyen31093.futurifysample.R
import com.github.minhnguyen31093.futurifysample.model.Offer
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_sample.*

class OfferAdapter(private val items: List<Offer>) : RecyclerView.Adapter<OfferAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sample, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    inner class ViewHolder(override val containerView: View?) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(item: Offer) {
            ivShare.setOnClickListener {  }
            ivHeart.setOnClickListener {  }

            Glide.with(ivItem.context.applicationContext).load(item.image).into(ivItem)

            tvTitle.text = item.name
            rbItem.rating = item.rating
            tvRating.text = tvRating.context.getString(R.string.format_reviews, item.reviews.toString())
            tvPrice.text = item.location
            tvLocation.text = item.locationDetail
        }
    }
}