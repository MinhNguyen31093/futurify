package com.github.minhnguyen31093.futurifysample.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.github.minhnguyen31093.futurifysample.R
import com.github.minhnguyen31093.futurifysample.fragment.offer.OfferFragment
import com.github.minhnguyen31093.futurifysample.fragment.pay.PayFragment
import com.github.minhnguyen31093.futurifysample.fragment.profile.ProfileFragment
import com.github.minhnguyen31093.futurifysample.fragment.wallet.WalletFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initEvent()
        setUpView()
    }

    private fun initEvent() {
        ivSetting.setOnClickListener { }
        ivCart.setOnClickListener { }

        ibScan.setOnClickListener { }
        tvScan.setOnClickListener { ibScan.performClick() }

        tvWallet.setOnClickListener { setSelected(0) }
        tvOffer.setOnClickListener { setSelected(1) }
        tvPay.setOnClickListener { setSelected(2) }
        tvProfile.setOnClickListener { setSelected(3) }
        vpMain.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                setSelected(position)
            }
        })
    }

    private fun setUpView() {
        val items = ArrayList<Fragment>()
        items.add(WalletFragment())
        items.add(OfferFragment())
        items.add(PayFragment())
        items.add(ProfileFragment())
        val adapter = MainPagerAdapter(items, supportFragmentManager)
        vpMain.adapter = adapter
        vpMain.currentItem = 1
    }

    private fun setSelected(position: Int) {
        tvWallet.isSelected = false
        tvOffer.isSelected = false
        tvPay.isSelected = false
        tvProfile.isSelected = false
        when (position) {
            0 -> tvWallet.isSelected = true
            1 -> tvOffer.isSelected = true
            2 -> tvPay.isSelected = true
            3 -> tvProfile.isSelected = true
        }
        if (vpMain.currentItem != position) {
            vpMain.currentItem = position
        }
    }
}
